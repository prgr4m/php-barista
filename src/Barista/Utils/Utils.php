<?php
namespace Barista\Utils;

class Utils
{
    const SEP = DIRECTORY_SEPARATOR; // save some typing

    static public function get_barista_home(): string
    {
        return getenv("HOME") . self::SEP . ".barista";
    }

    static public function init_check()
    {
        $home = self::get_barista_home();
        if(!file_exists($home))
        {
            if(!mkdir($home, 0755))
                die("Failed to create application home directory!" . PHP_EOL);
        }
    }

    static public function get_npm_prefix(): string
    {
        $prefix_cmd = "npm config get prefix";
        return trim(shell_exec($prefix_cmd));
    } 

    static public function make_npm_prefix(string $venv_name): string
    {
        $home = self::get_barista_home();
        return $home . self::SEP . $venv_name;
    }

    static public function get_node_modules_path(): string
    {
        $prefix = self::get_npm_prefix();
        return implode(self::SEP, [$prefix, 'lib', 'node_modules']);
    }

    static public function make_node_modules_path(string $venv_name): string
    {
        $new_prefix = self::make_npm_prefix($venv_name);
        return implode(self::SEP, [$new_prefix, 'lib', 'node_modules']);
    }

    static public function get_venv_list(): array
    {
        $home = Utils::get_barista_home();
        return array_slice(scandir($home), 2);
    }

    static public function is_activated_environment(): bool
    {
        $prefix = self::get_npm_prefix();
        $home = self::get_barista_home();
        return strpos($prefix, $home) !== false;
    }

    static public function is_same_environment(string $venv_name): bool
    {
        if (!self::is_activated_environment())
            return false;
        else
        {
            $requested_root = self::make_npm_prefix($venv_name);
            $current_root = self::get_npm_prefix();
            if ($requested_root == $current_root)
                return true;
            else
                return false;
        }
    }

    static public function get_system_node(): string
    {
        return trim(shell_exec('which node'));
    }

    static public function get_data_dir(): string
    {
        return realpath(dirname(__FILE__) . '/../../../data');
    }

    static public function get_activation_script(): string
    {
        $activation_script = self::get_data_dir() . self::SEP . 'activate';
        if (!file_exists($activation_script))
            die("Cannot find activation script template");
        return file_get_contents($activation_script);
    }

    static public function get_shim_script(): string
    {
        $shim_script = self::get_data_dir() . self::SEP . 'shim';
        if (!file_exists($shim_script))
            die("Cannot find shim script template");
        return file_get_contents($shim_script);
    }
}
