<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class ActivateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('env:activate')
            ->setDescription('generate activation code')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'name of nodejs virtual environment to activate'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $venv_name = $input->getArgument('name');
        $venvs = Utils::get_venv_list();
        if (count($venvs) > 0)
        {
            if (!in_array($venv_name, $venvs))
                $output->writeln("<error>nodejs virtualenv '$venv_name' doesn't exist</error>");
            else
            {
                // checking activation script
                $sep = DIRECTORY_SEPARATOR;
                $home = Utils::get_barista_home();
                $path = implode($sep, [$home, $venv_name, 'bin', 'activate']);

                if (!file_exists($path))
                    $output->writeln("<error>shell activation script missing from nodejs virtual environment</error>");
                else
                {
                    // descriptorspec just as in the shell
                    $proc_desc_spec = [
                        0 => ['pipe', 'r'],
                        1 => ['pipe', 'w'],
                        2 => ['file', '/tmp/barista-xclip-error.txt', 'a']
                    ];
                    $xclip_cmd = 'xclip -selection clipboard';
                    $xclip_proc = proc_open($xclip_cmd, $proc_desc_spec, $pipes, NULL, NULL);
                    if (is_resource($xclip_proc))
                    {
                        $activate_cmd = "source $path";
                        fwrite($pipes[0], $activate_cmd);
                        fclose($pipes[0]);
                        fclose($pipes[1]); // closing to avoid deadlock
                        proc_close($xclip_proc); // need return code?
                        $output->writeln("<info>Activation code copied to the clipboard</info>");
                        $output->writeln("press: <Shift+F10><P><Enter>");
                        $output->writeln("");
                        $output->writeln("<info>To deactivate an activated virtual environment run:</info>");
                        $output->writeln("deactivate_node");
                    }
                    else
                    {
                        $output->writeln("<error>xclip is not installed!</error>");
                        $output->writeln("install xclip through your package manager");
                    }
                }
            }
        }
        else
        {
            $output->writeln("<error>there aren't any nodejs virtual environments to activate</error>");
            $output->writeln("Please create one");
        }
    }
}
