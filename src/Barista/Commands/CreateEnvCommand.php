<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class CreateEnvCommand extends Command
{
    const SEP = DIRECTORY_SEPARATOR;

    protected function configure()
    {
        $this
            ->setName('env:create')
            ->setDescription('create a nodejs virtual environment')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'name of nodejs virtual environment to create'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $venv_name = $input->getArgument('name');
        $venv_root = Utils::make_npm_prefix($venv_name);
        if (file_exists($venv_root))
            $output->writeln("<error>nodejs virtual environment already exists</error>");
        elseif (Utils::is_activated_environment())
        {
            $output->writeln("<comment>You are running under an activated virtual environment</comment>");
            $output->writeln("To deactivate run: deactivate_node");
        }
        else
        {
            $setup_config = [
                'venv_name' => $venv_name,
                'venv_root' => $venv_root,
                'venv_bin' => implode(self::SEP, [$venv_root, 'bin']),
                'venv_modules' => Utils::make_node_modules_path($venv_name)
            ];
            $this->create_directories($setup_config);
            $this->setup_bin_directory($setup_config);

            $activate_cmd = $this->getApplication()->find('env:activate');
            $args = [
                'command' => 'env:activate',
                'name' => $venv_name
            ];
            $activate_input = new ArrayInput($args);
            // I don't need the return code for running the command
            $activate_cmd->run($activate_input, $output);
        }
    }

    private function create_directories(array $setup_config)
    {
        $src_dir = implode(self::SEP, [$setup_config['venv_root'], 'src']);
        if (!mkdir($setup_config['venv_modules'], 0755, true))
            die("Could not create virtual environment directory" . PHP_EOL);
        mkdir($setup_config['venv_bin'], 0755);
        mkdir($src_dir, 0755);
    }

    private function setup_bin_directory(array $setup_config)
    {
        $activate_filename = $setup_config['venv_bin'] . self::SEP . 'activate';
        $activate_contents = str_replace('BARISTA_ENV_PATH', $setup_config['venv_root'], Utils::get_activation_script());
        $activate_contents = str_replace('BARISTA_ENV_NAME', $setup_config['venv_name'], $activate_contents);
        $this->file_writer($activate_filename, $activate_contents);

        $shim_filename = $setup_config['venv_bin'] . self::SEP . 'shim';
        $shim_contents = str_replace('BARISTA_ENV_PATH', $setup_config['venv_root'], Utils::get_shim_script());
        $shim_contents = str_replace('BARISTA_ENV_MODULES_PATH', $setup_config['venv_modules'], $shim_contents);
        $shim_contents = str_replace('SYSTEM_NODE', Utils::get_system_node(), $shim_contents);
        $this->file_writer($shim_filename, $shim_contents);

        $node_filename = $setup_config['venv_bin'] . self::SEP . 'node';
        $this->file_writer($node_filename, $shim_contents);
        $node_link = $setup_config['venv_bin'] . self::SEP . 'nodejs';
        symlink($node_filename, $node_link);
        // user feedback here...
    }

    private function file_writer(string $filename, string $contents)
    {
        $fh = fopen($filename, 'w') or die("Could not create $filename" . PHP_EOL);
        fwrite($fh, $contents);
        fclose($fh);
        chmod($filename, 0755);
    }
}
