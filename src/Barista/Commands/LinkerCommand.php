<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class LinkerCommand extends Command
{
    protected function configure()
    {
        $this->setName("env:linker")
             ->setDescription("npm Linker utility");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (Utils::is_activated_environment())
        {
            $project_deps = $this->parse_package_json();
            $global_path = Utils::get_node_modules_path();
            $local_pkgs = array_keys($project_deps);

            // removing the '.' and '..' in the listing
            $global_listing = array_slice(scandir($global_path), 2);

            $missing_packages = array_diff($local_pkgs, $global_listing);
            if (count($missing_packages) > 0)
            {
                $output->writeln('<info>Installing missing packages...</info>');
                $global_install_cmd = "npm install -g";
                foreach ($missing_packages as $pkg)
                {
                    $pkg_install = $pkg . "@" . $project_deps[$pkg];
                    system("$global_install_cmd $pkg_install");
                }
            }
            $output->writeln('<info>Creating npm links...</info>');
            foreach ($local_pkgs as $pkg)
                system("npm link $pkg");
        }
        else
        {
            $msg = "<error>Run linker under an activated nodejs virtual environment</error>";
            $output->writeln($msg);
        }
    }

    private function parse_package_json($pkg='package.json')
    {
        if (!file_exists($pkg))
            die("$pkg file not present in current working directory". PHP_EOL);
        $pkg_contents = json_decode(file_get_contents($pkg), true);
        $deps = [];
        if (array_key_exists('dependencies', $pkg_contents))
        {
            $deps = array_merge($deps, $pkg_contents['dependencies']);
        }
        if (array_key_exists('devDependencies', $pkg_contents))
        {
            $deps = array_merge($deps, $pkg_contents['devDependencies']);
        }
        return $deps;
    }
}
