<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class ListEnvCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('env:list')
            ->setDescription('list available nodejs virtual environments');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $venvs = Utils::get_venv_list();
        if (count($venvs) > 0)
        {
            $output->writeln('<info>available nodejs venv(s):</info>');
            foreach ($venvs as $venv)
            {
                $output->writeln($venv);
            }
        }
        else
            $output->writeln('<info>no available nodejs virtual environments</info>');
    }
}
