<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class CloneCommand extends Command
{
    const SEP = DIRECTORY_SEPARATOR;

    protected function configure()
    {
        $this
            ->setName('env:clone')
            ->setDescription('clone an existing nodejs virtual environment')
            ->addArgument(
                'existing',
                InputArgument::REQUIRED,
                'name of existing nodejs virtual environment to clone')
            ->addArgument(
                'new_name',
                InputArgument::REQUIRED,
                'name of new nodejs virtual environment'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // need to check to see if the venv already exists
        $old_env = $input->getArgument('existing');
        $new_env = $input->getArgument('new_name');
        $old_env_path = Utils::make_npm_prefix($old_env);

        if (!file_exists($old_env_path))
        {
            $output->writeln("<error>nodejs virtual environment doesn't exist</error>");
        }
        else
        {
            $new_env_path = Utils::make_npm_prefix($new_env);
            $command = escapeshellcmd("cp -r $old_env_path $new_env_path");
            $output->writeln("<info>Cloning environment...</info>");
            system($command);
            $this->setup_bin_directory($new_env_path, $old_env, $new_env);
            $output->writeln("<info>Successfully cloned $old_env to $new_env</info>");

            $activate_cmd = $this->getApplication()->find('env:activate');
            $args = [
                'command' => 'env:activate',
                'name' => $new_env
            ];
            $activate_input = new ArrayInput($args);
            $activate_cmd->run($activate_input, $output);
        }
    }

    private function setup_bin_directory(string $venv_path, string $old_name, string $new_name)
    {
        $bin_dir = $venv_path . self::SEP . 'bin';
        $target_files = ['node', 'activate', 'shim'];
        foreach ($target_files as $target_file)
        {
            $file = $bin_dir . self::SEP . $target_file;
            $original_contents = file_get_contents($file);
            $contents = str_replace($old_name, $new_name, $original_contents);
            $fh = fopen($file, 'w');
            fwrite($fh, $contents);
            fclose($fh);
        }
    }
}
