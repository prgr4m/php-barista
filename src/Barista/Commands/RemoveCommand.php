<?php
namespace Barista\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Barista\Utils\Utils;

class RemoveCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('env:remove')
            ->setDescription('remove a nodejs virtual environment')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'name of nodejs virtualenv to delete'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $venv_name = $input->getArgument('name');

        if (Utils::is_same_environment($venv_name))
        {
            $output->writeln("<error>Cannot remove the current activated environment</error>");
            $output->writeln("Deactivate the environment by running the following command:");
            $output->writeln("\tdeactivate_node");
        }
        else
        {
            $venv_path = Utils::make_npm_prefix($venv_name);
            if (!is_dir($venv_path))
                $output->writeln("<error>nodejs virtual environment '$venv_name' doesn't exist</error>");
            else
            {
                exec("rm -rf $venv_path");
                $output->writeln("<info>removed nodejs virtual environment</info>");
            }
        }
    }
}
