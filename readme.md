# Barista

## Notice

This is a php port of the original found [here](https://bitbucket.org/prgr4m/barista-python).
This is also the currently maintained version as the other has been deprecated.

This is a personal project but if you stumble upon it, I hope you find it useful. 

## Introduction

Barista is a cli application for managing nodejs virtual environments, similar 
to python's virtualenv but is more appropriate for **prototyping** projects and 
conserving diskspace if you need to try out an idea but don't want to download 
half of the internet to do it.

## Requirements

- nodejs / [nvm](https://github.com/creationix/nvm)
- xclip (for clipboard/paste functionality in linux)
- bash (this is what I have/use for my shell)
- php (I'm using 7.0 + type hinting)

## Credit where credit is due

The shell activation + shim scripts were originally generated from the 
[nodeenv](https://github.com/ekalinin/nodeenv) project.

## License

Copyright (c) 2016 John Boisselle <prgr4m@yahoo.com>
 
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
